import React, { useState, useContext, useEffect } from "react"
import { useStaticQuery, graphql, Link } from "gatsby"
import Image from "gatsby-image"

import Head from "../components/head"
import Layout from "../components/layout"
import CategoriesList from "../components/categories_list"
import { GlobalDispatchContext } from "../context/GlobalContextProvider"
import useUniqueCategories from "../hooks/useUniqueCategories"

import "./blog.scss"

export default function BlogPage() {
  const data = useStaticQuery(graphql`
    query {
      allContentfulBlogPost(sort: { fields: publishedDate, order: DESC }) {
        edges {
          node {
            title
            slug
            category
            excerpt
            publishedDate(fromNow: true)
            cover {
              fluid(maxWidth: 600, quality: 90) {
                ...GatsbyContentfulFluid_withWebp
              }
            }
          }
        }
      }
    }
  `)

  const posts = data.allContentfulBlogPost.edges
  const dispatch = useContext(GlobalDispatchContext)
  const categories = useUniqueCategories(data.allContentfulBlogPost.edges)
  const [type, setType] = useState("")
  const [browserWidth, setBrowserWidth] = useState(window.innerWidth)

  useEffect(() => {
    dispatch({ type: "PAGE_NAME", page: "blog" })

    // detect browser width while it less than 1000px, remove hero post
    let resize

    window.addEventListener("resize", e => {
      clearTimeout(resize)
      resize = setTimeout(() => {
        setBrowserWidth(e.target.innerWidth)
      }, 100)
    })
  }, [dispatch])

  return (
    <Layout>
      <Head title="Blog" />
      <div className="blog-page-container">
        <CategoriesList categories={categories} type={type} setType={setType} />
        <ol className="posts">
          {posts &&
            posts
              .filter(({ node }) => {
                if (!type) return true

                return type === node.category
              })
              .map((edge, i) => {
                const {
                  title,
                  slug,
                  publishedDate,
                  category,
                  excerpt,
                  cover,
                } = edge.node

                return (
                  <Post
                    type={i === 0 && browserWidth > 1000 ? "hero" : "normal"}
                    image={cover.fluid}
                    slug={slug}
                    category={category}
                    publishedDate={publishedDate}
                    title={title}
                    excerpt={excerpt}
                    key={`post_${i}`}
                  />
                )
              })}
        </ol>
      </div>
    </Layout>
  )
}

function Post({ image, slug, category, publishedDate, title, excerpt, type }) {
  return (
    <li className={`post ${type === "hero" ? "hero-post" : "normal-post"}`}>
      <div className="image-container">
        <p className="info">
          <span className="category">{category}</span>
          <span>|</span>
          <span className="publish-date">{publishedDate}</span>
        </p>
        <Link to={`/blog/${slug}`}>
          <Image fluid={image} />
        </Link>
      </div>
      <div className="content-container">
        <Link to={`/blog/${slug}`}>
          <h3 className="title">{title}</h3>
        </Link>
        <p className="excerpt">{excerpt}</p>
        <Link to={`/blog/${slug}`} className="continue-reading">
          Continue Reading
        </Link>
      </div>
    </li>
  )
}
