import React, { useContext, useEffect } from "react"
import { graphql } from "gatsby"
import RichTextRenderer from "../components/contentful_rich_text_renderer"

import { GlobalDispatchContext } from "../context/GlobalContextProvider"
import Layout from "../components/layout"
import Head from "../components/head"
import PrevNxtTemplate from "../components/prev_next_template"
import "./article.scss"

export const query = graphql`
  query($slug: String!) {
    contentfulBlogPost(slug: { eq: $slug }) {
      title
      category
      publishedDate(formatString: "YYYY-MM-DD")
      body {
        json
      }
    }
  }
`

export default function Blog({ data, pageContext }) {
  const { title, category, publishedDate, body } = data.contentfulBlogPost
  const dispatch = useContext(GlobalDispatchContext)

  useEffect(() => {
    dispatch({ type: "PAGE_NAME", page: "blog-post" })
  }, [dispatch])

  return (
    <Layout>
      <Head title={title} />
      <article className="article">
        <h1 className="title">{title}</h1>
        <p className="article-info">
          <span>on </span>
          <span className="publish-date">{publishedDate}</span>
          <span> in </span>
          <span className="category">{category}</span>
        </p>
        <RichTextRenderer richTextJson={body.json} />
        <PrevNxtTemplate pageContext={pageContext} />
      </article>
    </Layout>
  )
}
