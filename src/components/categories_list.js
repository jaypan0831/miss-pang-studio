import React from "react"

import "./categories_list.scss"

export default function CategoriesList({ categories, type, setType }) {
  return (
    <aside className="categories">
      <button
        className={`filter-btn link-button ${!type ? "current" : ""}`}
        onClick={() => setType("")}
      >
        ALL
      </button>
      {categories &&
        categories.map((cat, i) => (
          <button
            key={`cat_link_${i}`}
            className={`filter-btn link-button ${
              type === cat ? "current" : ""
            }`}
            onClick={() => setType(cat)}
          >
            {cat}
          </button>
        ))}
    </aside>
  )
}
