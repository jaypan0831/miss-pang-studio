import React from "react"
import { Link } from "gatsby"

import "./prev_next_template.scss"

export default function PrevNxtTemplate({ pageContext }) {
  const { next, prev } = pageContext

  return (
    <div className="article-nav-wrapper">
      {prev && (
        <Link to={`/blog/${prev.slug}`} className="nav-prev">
          <div className="nav-prev-title">{"< Privious Reading"}</div>
          <span>{prev.title}</span>
        </Link>
      )}
      {next && (
        <Link to={`/blog/${next.slug}`} className="nav-next">
          <div className="nav-next-title">Next Reading ></div>
          <span>{next.title}</span>
        </Link>
      )}
    </div>
  )
}
