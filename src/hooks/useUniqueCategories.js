import React from "react"

export default function useUniqueCategories(array) {
  const [uniqueArray, setUniqueArray] = React.useState([])
  React.useEffect(() => {
    let uniqueCat = []

    // store all unique categories
    array.forEach(({ node }) => {
      if (node.category && !uniqueCat.includes(node.category)) {
        uniqueCat.push(node.category)
      }
    })

    setUniqueArray(uniqueCat)
  }, [array])

  return uniqueArray
}
